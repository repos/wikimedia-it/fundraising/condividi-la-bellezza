<?php
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

require './libs/Exception.php';
require './libs/PHPMailer.php';
require './libs/SMTP.php';

require 'config.php';

$mail = new \PHPMailer\PHPMailer\PHPMailer();


$email_body = '';

$dest_email = $_POST["email_destinatario"];
$dest_name = $_POST["nome_destinatario"];
$mit_name = $_POST["nome_mittente"];
$mit_surname = $_POST["cognome_mittente"];
$mit_email = $_POST["email_mittente"];
$mit_msg = $_POST["messaggio_email"];
$card_id = $_POST["card_id"];
$privacy = $_POST["privacy"];
$newsletter = $_POST["newsletter"];


$card_teaser = $cartoline[$card_id]['img_teaser'];
$card_full = $cartoline[$card_id]['img_full'];
$card_title= $cartoline[$card_id]['title'];
$card_subject = $cartoline[$card_id]['subject'];
$card_auth_name = $cartoline[$card_id]['auth_name'];
$card_licence = $cartoline[$card_id]['licence'];
$card_where = $cartoline[$card_id]['where'];
$card_award = $cartoline[$card_id]['award'];



require './theme/email.php';

//$logfile = '../logfile_cartoline_'.date('Y-m-d').'.csv';
$logfile = '../logfile_cartoline.csv';



//$email_body .= require './theme/email.php';


$email_body .= "\n\n<br/>";
$email_body .=  "destinatario nome: $dest_name \n<br/>";
$email_body .=  "destinatrio email: $dest_email\n<br/>";
$email_body .=  "\n<br/><br/>";

$email_body .=  "mittente nome: $mit_name\n<br/>";
$email_body .=  "messaggio: $mit_msg\n<br/>";

$email_body .= "\n\n<br/><br/>";

$mail->isSMTP();
$mail->Host = SMTP_HOSTNAME;
$mail->SMTPAuth = SMTP_AUTH;
$mail->Username = SMTP_USERNAME;
$mail->Password = SMTP_PASSWORD;
$mail->SMTPSecure = SMTP_SECURE;
$mail->Port = SMTP_PORT;

$mail->setFrom( EMAIL_FROM_ADDR, EMAIL_FROM_NAME );
$mail->addReplyTo( EMAIL_REPLYTO_ADDR, EMAIL_REPLYTO_NAME );
$mail->addAddress($dest_email, '');
//$mail->addBCC('bcc1@example.com', 'Alex');

$mail->Subject = $mit_name. ' ti ha inviato una cartolina';
$mail->CharSet = 'UTF-8';
$mail->Body = $email_body;
$mail->msgHTML($email_html);


if($mail->send()){
  //echo 'invio effettuato';
}else{
  // echo 'Message could not be sent.';
  // echo 'Mailer Error: ' . $mail->ErrorInfo;
}



$output = "
  \n\t<h1>Grazie per la tua condivisione!</h1>
  \n\t<p class='thumbs-up text-center'><i class='far fa-thumbs-up'></i></p>
  \n\t<p>".$mit_name.",  grazie per aver inviato una cartolina a ".$dest_name."</p>
  \n\t<p>Condividere la bellezza è sempre un gesto rivoluzionario</p>
";


echo '<div id="response_wrapper">';
echo '<div id="response_inner">';
echo $output ;
echo '</div>';
echo '</div>';


if(!is_file($logfile)){
  $writetolog = '"Date","Destinatario Email","Mittente Nome","Mittente Cognome","Mittete Email","Cartolina","Privacy","Newsletter"'."\n";
  file_put_contents($logfile, $writetolog);
}

$writetolog = file_get_contents($logfile);
if ($writetolog == ''){
  $writetolog = '"Date","Destinatario Email","Mittente Nome","Mittente Cognome","Mittete Email","Cartolina","Privacy","Newsletter"'."\n";
}
$newline = date('Y-m-d--H-i').',"'.$dest_email.'","'.$mit_name.'","'.$mit_surname.'","'.'","'.$mit_email.'","'.$card_full.'","'.$privacy.'","'.$newsletter."\"\n";
$writetolog .= $newline;
file_put_contents($logfile, $writetolog);
