<footer>
  <div class="container">
    <div class="row">
      <div class="col-12 col-md-4 col-left">
        <div class="top">
          <h6>Wikimedia Italia</h6>
          <p>Associazione per la diffusione della conoscenza libera</p>
        </div>
        <div class="center">
          <p>Via Bergognone, 34 - 20144 Milano (MI)<br>P.Iva 05599740965 - CF 94039910156<br>Tel. (+39) 02 97677170 - Fax. (+39) 02 56561506<br><a href="mailto:segreteria@wikimedia.it">segreteria@wikimedia.it</a></p>
        </div>
        <div class="bottom">
          <?php if( !empty( $page_footer_link ) ): ?>
            <?php echo $page_footer_link ?>
          <?php endif ?>
          <h6><a href="https://wiki.wikimedia.it/index.php/Informativa_sulla_privacy" target="_blank">Informativa sulla privacy</a></h6>
          <h6><a href="https://www.wikimedia.it/cookie-policy/" target="_blank">Cookie Policy</a></h6>
          <h6><a href="https://emeraldcommunication.com/" target="_blank">Web Agency</a></h6>
        </div>
      </div>
      <div class="col-12 col-md-4 col-center">
        <div class="socials">
          <div class="title">
            <h6>Seguici su</h6>
          </div>
          <div class="icons">
            <div class="fb">
              <a href="https://www.facebook.com/Wikimedia.Italia/" target="_blank"><i class="fab fa-facebook-f"></i></a>
            </div>
            <div class="ig">
              <a href="https://www.instagram.com/p/CWGoLV8KVAu/" target="_blank"><i class="fab fa-instagram"></i></a>
            </div>
            <div class="twitter">
              <a href="https://twitter.com/WikimediaItalia" target="_blank"><i class="fab fa-twitter"></i></a>
            </div>
            <div class="yt">
              <a href="https://www.youtube.com/user/wikimediaitalia" target="_blank"><i class="fab fa-youtube"></i></a>
            </div>
          </div>
        </div>
      </div>
      <div class="col-12 col-md-4 col-right">
        <h4>SOSTIENI WIKIMEDIA ITALIA</h4>
        <div class="donate-link">
          <a href="https://sostieni.wikimedia.it/dai_valore_alla_bellezza/" target="_blank" class="bg_red">DONA ORA</a>
        </div>
      </div>
    </div>
  </div>
</footer>


    <script src="/theme/jquery.min.js"></script>
    <script src="/theme/bootstrap.min.js"></script>
    <script src="/theme/popper.min.js"></script>
    <script src="/theme/bootstrap.bundle.min.js"></script>


    <script src="/theme/global.js"></script>
  </body>
</html>
